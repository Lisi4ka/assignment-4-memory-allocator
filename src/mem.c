#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

extern inline bool region_is_invalid(const struct region *r);
static bool try_merge_with_next(struct block_header *block);


static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    /*  ??? */
    static struct block_header blockHeader;
    if (query == REGION_MIN_SIZE) {
        query = REGION_MIN_SIZE + getpagesize();
    }
    query = region_actual_size(query);

    void *mMap = map_pages(addr, query, MAP_FIXED);
    struct region result = {0};
    result.extends = false;
    if (mMap == MAP_FAILED) {
        mMap = map_pages(addr, query, 0);
        if (mMap != MAP_FAILED) {
            block_init(mMap, (block_size) {query}, NULL);
            result.addr = mMap;
            result.size = query;
            return result;
        }


        return REGION_INVALID;
    }
    block_init(mMap, (block_size) {query}, NULL);

    result.addr = mMap;
    if (addr == mMap) {
        result.extends = true;
    }
    result.size = query;
    return result;
}


static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    /*  ??? */
    struct block_header *blockHeader;
    struct block_header *blockHeaderProm;
    blockHeader = HEAP_START;

    while (blockHeader != NULL) {
        _free(blockHeader->contents);
        blockHeader = blockHeader->next;
    }
    blockHeader = HEAP_START;
    while (blockHeader != NULL) {
        while (try_merge_with_next(blockHeader)) {}
        blockHeaderProm = blockHeader->next;
        munmap(blockHeader, round_pages(size_from_capacity(blockHeader->capacity).bytes));
        blockHeader = blockHeaderProm;
    }


}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    /*  ??? */
    query = size_max(query, BLOCK_MIN_CAPACITY);
    query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
    if (!block_splittable(block, query)) {
        return false;
    }
    for (size_t i = 0; i < block->capacity.bytes; i++) {
        block->contents[i] = 0;
    }
    struct block_header *originalNext = block->next;
    size_t originalSize = size_from_capacity(block->capacity).bytes;
    struct block_header *blockNextHeader = (struct block_header *) ((void *) (block->contents) + query);
    block->next = blockNextHeader;
    block->capacity.bytes = query;
    block->is_free = true;
    blockNextHeader->capacity.bytes = (size_t) (originalSize - query - 2 * offsetof(struct block_header, contents));
    blockNextHeader->is_free = true;
    blockNextHeader->next = originalNext;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst != NULL && snd != NULL && fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    /*  ??? */
    if (!mergeable(block, block->next)) {
        return false;
    }
    struct block_header *blockHeader = block->next;
    block->next = block->next->next;
    block->capacity.bytes += size_from_capacity(blockHeader->capacity).bytes;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    /*??? */
    
    struct block_search_result blockSearchResult;
    if (block == NULL) {
        blockSearchResult.type = BSR_CORRUPTED;
        blockSearchResult.block = NULL;
        return blockSearchResult;
    }

    while (block->next != NULL) {

        while (try_merge_with_next(block)) {}
        if (block_is_big_enough(sz, block) && block->is_free) {
            blockSearchResult.type = BSR_FOUND_GOOD_BLOCK;
            blockSearchResult.block = block;
            return blockSearchResult;
        }
        block = block->next;
    }
    if (block_is_big_enough(sz, block) && block->is_free) {
        blockSearchResult.type = BSR_FOUND_GOOD_BLOCK;
        blockSearchResult.block = block;
        return blockSearchResult;
    }
    blockSearchResult.type = BSR_REACHED_END_NOT_FOUND;
    blockSearchResult.block = block;
    return blockSearchResult;

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
    struct block_search_result blockSearchResult;
    blockSearchResult = find_good_or_last(block, query);
    if (blockSearchResult.type == BSR_REACHED_END_NOT_FOUND) {
        return blockSearchResult;
    }
    split_if_too_big(blockSearchResult.block, query);

    blockSearchResult.block->is_free = false;
    return blockSearchResult;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) {
        return NULL;
    }
    void *blockAfter = block_after(last);
    struct region region = alloc_region(blockAfter, query);
    if (region_is_invalid(&region)) {
        return NULL;
    }
    last->next = region.addr;
    try_merge_with_next(last);
    if (last->next == NULL) {
        return last;
    }
    return last->next;
}


/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
    struct block_search_result blockSearchResult;
    /*  ??? */
    blockSearchResult = try_memalloc_existing(query, heap_start);
    //assert(!true);
    if (blockSearchResult.type == BSR_FOUND_GOOD_BLOCK) {
        blockSearchResult.block->is_free = false;
        return blockSearchResult.block;
    }
    if (blockSearchResult.type == BSR_CORRUPTED) {
        return NULL;
    }
    struct block_header *blockHeader;
    blockHeader = grow_heap(blockSearchResult.block, query);
    if (!blockHeader) {
        return NULL;
    }
    return try_memalloc_existing(query, blockHeader).block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);

    if (addr) {
        return addr->contents;
    } else { return NULL; }
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header)) {}
    /*  ??? */
}
